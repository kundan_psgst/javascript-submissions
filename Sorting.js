data = require("./Registrations.json");


data_bcomm=[]
data_cs=[]

for(i of data)
{
    if(i.courseName == "BSc Computer Technology")
    {
        data_cs.push(i);
    }
    else{
        data_bcomm.push(i);
    }
}
function first_order(a,b){
    if(a.board == "TNHSE"){
        if(b.board == "TNHSE"){
            if(a.cutoff>b.cutoff)
            {
                return -1;
            }
            else if(a.cutoff == b.cutoff){
                date_1 = a.dobStr.split("/").reverse();
                date_2 = b.dobStr.split("/").reverse();
                if(date_1>date_2){return 1;}
                else if(date_1==date_2){
                    if(a.courseRegNo > b.courseRegNo){return -1;}
                    else{return 1;}
                }
                else{return -1;}
            }
            else{ return 1;}
        }
        return -1;
    }
    else{
        return 1;
    }
}

function second_order(a,b){
    if(a.board != "OTHER"){
        if(b.board == "TNHSE"){
            if(a.cutoff>b.cutoff)
            {
                return -1;
            }
            else if(a.cutoff == b.cutoff){
                date_1 = a.dobStr.split("/").reverse();
                date_2 = b.dobStr.split("/").reverse();
                if(date_1>date_2){return 1;}
                else if(date_1==date_2){
                    if(a.courseRegNo > b.courseRegNo){return -1;}
                    else{return 1;}
                }
                else{return -1;}
            }
            else{ return 1;}
        }
        return -1;
    }
    else{
        return 1;
    }
}


data_cs.sort(second_order);
data_bcomm.sort(first_order);
console.log("Data Computer Sci",data_cs);
console.log("Data Bcomm",data_bcomm);