function asynccall1(){
    return "Hey there";
}

function asynccall2(){
    return "Welcome to";
}
function computation1(str){
    return str + " PSG ST";
}

function computation2(str1,str2){
    return str1+", "+str2;
}

let promise = new Promise((res,rej)=>{
    res(asynccall2());
});

another_promise = promise.then(
    result => {
        return new Promise((res,rej)=> res(computation1(result)));
    },
);

let promise2 = new Promise((res,rej)=>{
    res(asynccall1());
});

Promise.all([promise2,another_promise]).then(ans => {for(i of ans){console.log(i);}})